#include "TrigTRT_TrackExtensionTool/TrigTRT_TrackExtensionTool.h"
#include "TrigTRT_TrackExtensionTool/TrigTRT_DetElementRoadTool.h"
#include "TrigTRT_TrackExtensionTool/TrigMagneticFieldTool.h"
#include "TrigTRT_TrackExtensionTool/TrigTRT_CombinedExtensionTool.h"

DECLARE_COMPONENT( TrigTRT_TrackExtensionTool )
DECLARE_COMPONENT( TrigTRT_DetElementRoadTool )
DECLARE_COMPONENT( TrigMagneticFieldTool )
DECLARE_COMPONENT( TrigTRT_CombinedExtensionTool )

